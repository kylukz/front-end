FROM node:15
WORKDIR /app
ADD . .
RUN npm install

RUN apt update -y 
RUN npm install axios -y 
RUN npm install md5 -y
RUN npm install -g @vue/cli
RUN vue add @vue/cli-service



EXPOSE 3000
CMD ["npm", "run", "serve"]
